/**
 * @link https://github.com/ncb000gt/node.bcrypt.js
 * @link http://stackoverflow.com/questions/26643587/bcrypt-hashes-generated-by-php-failing-in-node-js
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const bcrypt = require('bcryptjs');

/**
 * Convert a hash to format that is compatible with the PHP version of bcrypt.
 *
 * @param {string} hash
 * @returns {string}
 */
function toPhpCompatible(hash: string) {
	return hash.replace(/^\$2a\$/, '$2y$');
}

/**
 * Convert a hash from one generated by the PHP version of bcrypt.
 *
 * @param {string} hash
 * @returns {string}
 */
function fromPhpCompatible(hash: string) {
	return hash.replace(/^\$2y\$/, '$2a$');
}

export type HashOptions = {
	php?: boolean;
}

/**
 * BCrypt hashing class.
 */
export class Hash {
	private options: HashOptions;

	/**
	 * @param {HashOptions} options
	 * @constructor
	 */
	constructor(options?: HashOptions) {
		this.options = options || {
				php: false
			};
	}

	/**
	 * Create a hash.
	 *
	 * @param text
	 * @param iterations
	 * @returns {Promise}
	 */
	public create(text: string, iterations?: number): Promise<string> {
		iterations = iterations || 10;

		return new Promise((resolve, reject) => {
			bcrypt.genSalt(iterations, (err1: Error, salt: string) => {
				if (err1) {
					return reject(err1);
				}

				bcrypt.hash(text, salt, (err2: Error, hash: string) => {
					if (err2) {
						return reject(err2);
					}

					if (this.options.php === true) {
						hash = toPhpCompatible(hash);
					}

					resolve(hash);
				});
			});
		});
	};

	public compare(text: string, hash: string): Promise<boolean> {
		return new Promise((resolve, reject) => {
			if (this.options.php === true) {
				hash = fromPhpCompatible(hash);
			}

			bcrypt.compare(text, hash, (err: Error, result: boolean) => {
				if (err) {
					return reject(err);
				}

				resolve(result);
			});
		});
	};
}

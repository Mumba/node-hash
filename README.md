# Mumba Hash

Hashing with Promise support.

## Installation

```sh
$ npm install --save mumba-hash
```

## Examples

Normal usage:

```js
import {Hash} from "mumba-hash";

let hash = new Hash();

// Create a hash on "foo" with the default 5 iterations.
hash.create('foo')
	.then(function (hash) {
		// This will give us a "$2a$05..." type of hash.
		console.log(hash);
	})
	.catch(console.error);
```

PHP compatible mode:

```js
let hash = new Hash({ php: true });

// Create a hash on "foo" with the default 5 iterations.
hash.create('foo')
	.then(function (hash) {
		// This will give us a "$2y$05..." type of hash.
		console.log(hash);
	})
	.catch(console.error);
```

Comparing hashes with submitted text (such as a passphrase):

```js
let storedHash = '$2a$05$tpkxsCGs3DQAulwg8jMLm.2m6onpogPyISQUltka1LLUBgQ9EO1xG';

hash.compare('foo', storedHash)
	.then(function (result) {
		console.log(result ? "It's a match" : "This is not a match";
	});
```

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm install
$ npm test
```

## People

The original author of _Mumba Hash_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

[List of all contributors](https://gitlab.com/Mumba/node-hash/graphs/master)

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2016 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.


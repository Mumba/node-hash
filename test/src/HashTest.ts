/**
 * Hash tests.
 *
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from "assert";
import {Hash} from "../../src/index";

describe('Standard Mode', () => {
	let hash: Hash;

	beforeEach(() => {
		hash = new Hash();
	});

	it('create should hash some text', () => {
		return hash.create('foo')
			.then(function (hash: string) {
				assert.equal(hash.substr(0, 6), '$2a$10');
			});
	});

	it('create should be able to use a custom limit', () => {
		return hash.create('foo', 4)
			.then(function (hash: string) {
				assert.equal(hash.substr(0, 6), '$2a$04');
			});
	});

	it('should reject if bcrypt errors creating a hash', (done) => {
		hash.create('foo', <any>'not-a-number')
			.catch((err: Error) => {
				assert(/Illegal arguments: string/.test(err.message), 'should error due to non-integer iterations');
				done();
			})
			.catch(done);
	});

	it('compare should validate text against a good hash', () => {
		return hash.compare('foo', '$2a$05$tpkxsCGs3DQAulwg8jMLm.2m6onpogPyISQUltka1LLUBgQ9EO1xG')
			.then(function (result: boolean) {
				assert.equal(result, true);
			});
	});

	it('compare should invalidate text against a bad hash', () => {
		return hash.compare('foo', '$2a$05$aaaaaaaaaaaaaaaaaaaaa.2m6onpogPyISQUltka1LLUBgQ9EO1xG')
			.then(function (result) {
				assert.equal(result, false);
			});
	});

});

describe('PHP Compatible', () => {
	let hash: Hash;

	beforeEach(() => {
		hash = new Hash({ php: true });
	});

	it('create should hash some text', () => {
		return hash.create('foo')
			.then(function (hash) {
				assert.equal(hash.substr(0, 6), '$2y$10');
			});
	});

	it('compare should validate text against a good hash', () => {
		return hash.compare('foo', '$2y$05$tpkxsCGs3DQAulwg8jMLm.2m6onpogPyISQUltka1LLUBgQ9EO1xG')
			.then(function (result) {
				assert.equal(result, true);
			});
	});

	it('compare should invalidate text against a bad hash', () => {
		return hash.compare('foo', '$2y$05$aaaaaaaaaaaaaaaaaaaaa.2m6onpogPyISQUltka1LLUBgQ9EO1xG')
			.then(function (result: boolean) {
				assert.equal(result, false);
			});
	});

});
